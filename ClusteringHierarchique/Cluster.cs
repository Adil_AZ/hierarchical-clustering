﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClusteringHierarchique
{
    public class Cluster
    {
        public List<int> Points;
        public PointF Center;
        private List<Point> AllPoints;

        public Cluster(List<Point> points)
        {
            Points = new List<int>();
            Center = new PointF(0, 0);
            AllPoints = points;
        }

        //Calcule center
        public void CalculeCenter()
        {
            float x = 0;
            float y = 0;
            for (int i = 0; i < Points.Count; i++)
            {
                x += AllPoints[Points[i]].X;
                y += AllPoints[Points[i]].Y;
            }
            x = x / Points.Count;
            y = y / Points.Count;

            Center.X = x;
            Center.Y = y;
        }

    }
}
