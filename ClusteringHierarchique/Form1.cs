﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClusteringHierarchique
{
    public partial class Form1 : Form
    {
        List<Point> AllPoints;
        List<Cluster> AllClusters;
        PointF Center;

        public Form1()
        {
            InitializeComponent();

            AllPoints = new List<Point>();
            AllClusters = new List<Cluster>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //creation des cluster initial
            for (int i=0;i<AllPoints.Count;i++)
            {
                Cluster c = new Cluster(AllPoints);
                c.Points.Add(i);
                c.Center.X = AllPoints[i].X;
                c.Center.Y = AllPoints[i].Y;

                AllClusters.Add(c);
            }

            //boucle principale 2----3
            int k = Convert.ToInt32(numClust.Text);
            while (AllClusters.Count != k)
            {
                int c1, c2;
                GetCandidat(out c1, out c2);

                //fusion
                Cluster c = new Cluster(AllPoints);
                c.Points.AddRange(AllClusters[c1].Points);
                c.Points.AddRange(AllClusters[c2].Points);
                c.CalculeCenter();
                if (c1<c2)
                {
                    AllClusters.RemoveAt(c2);
                    AllClusters.RemoveAt(c1);
                }
                else
                {
                    AllClusters.RemoveAt(c1);
                    AllClusters.RemoveAt(c2);
                }
                AllClusters.Add(c);
            }
            panel1.Invalidate(true);
        }

        private void GetCandidat(out int c1, out int c2)
        {
            int imin = -1;
            int jmin = -1;
            float dmin = float.MaxValue;
            for (int i = 0; i < AllClusters.Count-1; i++)
            {

                for (int j = i+1; j < AllClusters.Count; j++)
                {
                    float d = Distance(i, j);
                    if(d<dmin)
                    {
                        imin = i;
                        jmin = j;
                        dmin = d;
                    }
                }
            }
            c1 = imin;
            c2 = jmin;
        }

        private float Distance(int c1, int c2)
        {
            return (float)Math.Sqrt(
                Math.Pow((AllClusters[c1].Center.X-AllClusters[c2].Center.X), 2)
                + Math.Pow((AllClusters[c1].Center.Y - AllClusters[c2].Center.Y), 2)
                ) ;
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Point p = new Point(e.X, e.Y);
            AllPoints.Add(p);
            Cluster c = new Cluster(AllPoints);
            c.Points.Add(AllPoints.Count-1);
            c.Center.X = p.X;
            c.Center.Y = p.Y;
            AllClusters.Add(c);
            panel1.Invalidate(true); // pour forcer la refli
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Color[] colors =
            {
                Color.Red,
                Color.Blue,
                Color.Green,
                Color.Yellow,
                Color.Pink,
                Color.Violet
            };

            Graphics g;
            g = Graphics.FromHwnd(panel1.Handle);
            for (int i = 0; i < AllClusters.Count; i++)
            {
                Pen pen;
                if (AllClusters.Count <= 6)
                    pen = new Pen(colors[i]);
                else
                    pen = new Pen(Color.White);

                for (int j = 0; j < AllClusters[i].Points.Count; j++)
                {
                    int index = AllClusters[i].Points[j];
                    Rectangle rect = new Rectangle(AllPoints[index].X - 3, AllPoints[index].Y - 3,6,6);
                    g.DrawEllipse(pen, rect);
                }
                pen.Dispose();
            }

           /* Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Red);
            g.DrawLine(pen, new Point(13, 13), new Point(20, 20));*/
            
        }
    }
}
